console.log('repeticao while')
let numero = 0;
while(numero<=10){
    if(numero%2==0){
        console.log(`valor nr -  ${(numero)} é par!`);
    }else{
        console.log(`valor nr -  ${(numero)}`);
    }
    numero++;
}
console.log('repeticao dowhile');
let numero1 = 0;
do {
    console.log(`valor nr - ${(numero1)}`)
    if(numero%2==0){
        console.log(`valor nr -  ${(numero1)} é par!`);
    }else{
        console.log(`valor nr -  ${(numero1)}`);
    }
    numero++
    numero1++;
} while (numero1<=0);
console.log('repeticao for');
for(let numero2 = 0; numero2<=10; numero2++){
    if(numero%2==0){
        console.log(`valor nr -  ${(numero2)} é par!`);
    }else{
        console.log(`valor nr -  ${(numero2)}`);
    }
    numero++
};